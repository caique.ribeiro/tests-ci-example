## Como inicializar a aplicação

### Desenvolvimento

1. Começe instalando as dependências do projeto com `npm`
2.  `npx run cypress:open` (rodar com auxilio de UI e visualicao no Browser)
3.  `npx run cypress:run` (rodar "headless" via terminal sem visualizacao de UI)


## Documentação Cypress

Se estiver desenvolvendo novas funcionalidades e está em busca de maior orientação sobre os comandos utilizados neste exemplo, não se esqueça de acessar a [documentação oficial](https://docs.cypress.io/api/table-of-contents) Cypress.io.

## Ferramental

### [Cypress.io](https://docs.cypress.io/)

Biblioteca dedicada para realização de testes em UI.


-----

# Regression Tests
Documentacao dos cenarios
https://docs.google.com/spreadsheets/d/1EDQihaEKp3_Da95ItmOy7NhH3AVp_ut0sR7MXX89VjY/edit#gid=0

1. [x] - Usuarios
2. [x] - Rentabilidade
3. [x] - Informe Diario
4. [ ] - Fundos
