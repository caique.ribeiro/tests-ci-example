import { defineConfig } from "cypress"

export default defineConfig({
   viewportWidth: 1920,
   viewportHeight: 1080,
   experimentalStudio: true,
   chromeWebSecurity: false,
   env: {
      ANBIMA_DEV: "https://hubanbima-dev.rtm.net.br",
      ANBIMA_UAT: "https://hubanbima-uat.rtm.net.br/",
      ANBIMA_CERT: "https://hubanbima-cert.rtm.net.br",
      INFORMES_DIARIOS:
         "https://hub-uat-hubfundos.rtm.net.br/anbima-request/v1/informesDiarios",
      SOLICITACOES:
         "https://hub-uat-hubfundos.rtm.net.br/anbima-request/v1/solicitacao?*",
   },
   e2e: {},
})
