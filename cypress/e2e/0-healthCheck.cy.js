import users from "../fixtures/users.json"

describe("HealthCheck", () => {
   it("Check UAT", () => {
      cy.visit(Cypress.env("ANBIMA_UAT"))

      cy.url().should("include", "https://iam-uat-hubanbima.rtm.net.br/realms/anbima-hub/")
      cy.get(".description").should("contain", "Insira o seu email e senha para entrar")
   })

   it("Check DEV", () => {
      cy.visit(Cypress.env("ANBIMA_DEV"))
      cy.url().should("include", "https://iam-dev-hubanbima.rtm.net.br/realms/anbima-hub/")
      cy.get(".description").should("contain", "Insira o seu email e senha para entrar")
   })

   it("Check CERT", () => {
      cy.visit(Cypress.env("ANBIMA_CERT"))
      cy.url().should("include", "https://iam-cert-hubanbima.rtm.net.br/realms/anbima-hub/")
      cy.get(".description").should("contain", "Insira o seu email e senha para entrar")
   })
})

describe("HealthCheck - IAM -", () => {
   it("Check IAM - UAT", () => {
      cy.visit(Cypress.env("ANBIMA_UAT"))
      cy.get("#username").type(users.user1.login)
      cy.get("#password").type(users.user1.password)
      cy.get("#kc-login").click()

      cy.get("form").should("contain", "Insira o código que você recebeu em seu email.")
      cy.wait(1000)
      cy.get('input[name="code"]').type("YEWBIGIZ")
      cy.contains("Autenticar").click()
      cy.url().should("include", "https://hubanbima-uat.rtm.net.br/")
      console.log("IAM - UAT - OK")
   })
})

describe("HealthCheck - Services", () => {
   it("Check Servicos", () => {
      cy.visit(Cypress.env("ANBIMA_UAT"))
      cy.loginCompleto(users.user1.login, users.user1.password)
      const urls = [
         {
            url: "https://hub-uat-hubfundos.rtm.net.br/anbima-auth/health",
            name: "/Auth",
         },
         {
            url: "https://hub-uat-hubfundos.rtm.net.br/anbima-request/health",
            name: "/Request",
         },
      ]
      urls.forEach((service) => {
         cy.wait(1000)
         cy.request("GET", service.url).should((res) => {
            assert.deepEqual(res.body, { health: "ok" })
         })
      })
   })
})
