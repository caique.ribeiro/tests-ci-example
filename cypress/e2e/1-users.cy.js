import users from "../fixtures/users.json"

beforeEach(() => {
   cy.visit(Cypress.env("ANBIMA_UAT"))
   cy.loginCompleto(users.user1.login, users.user1.password)
   cy.wait(1000)
})

it("CADASTRO USUARIO", () => {
   cy.get('.user-container > [data-testid="ExpandMoreOutlinedIcon"]').click({
      force: true,
   })
   cy.wait(1000)
   cy.contains("Administrativo").click({ force: true })
   cy.contains("Usu").click({ force: true })

   cy.contains("Novo usuário").click()

   cy.get('input[name="login"]').type(users.userCreate.login)
   cy.get('input[name="nome"]').type(users.userCreate.name)
   cy.get('input[name="telefoneComercial"]').type(users.userCreate.telefone)

   cy.contains("Cadastrar").click({ force: true })

   cy.wait(1000)
})

it("CADASTRO USUARIO JA EXISTENTE", () => {
   cy.on("uncaught:exception", () => false)

   cy.get('.user-container > [data-testid="ExpandMoreOutlinedIcon"]').click({
      force: true,
   })
   cy.contains("Administrativo").click({ force: true })
   cy.contains("Usu").click({ force: true })

   cy.contains("Novo usuário").click({ force: true })

   cy.get('input[name="login"]').type("caique.ribeiro@linainfratech.com.br")
   cy.get('input[name="nome"]').type(users.userCreate.name)
   cy.get('input[name="telefoneComercial"]').type(users.userCreate.telefone)

   cy.intercept("POST", "https://hub-uat-hubfundos.rtm.net.br/anbima-auth/v1/users").as("postRequest")
   cy.wait(1000)

   cy.contains("Cadastrar").click({ force: true })

   cy.wait("@postRequest").then((interception) => {
      expect(interception.response.statusCode).to.eq(400)
      cy.get(".go1888806478 > .MuiPaper-root").should("contain", "Usuário já existe na plataforma")
   })
   cy.wait(1000)
})
