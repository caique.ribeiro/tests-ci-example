Cypress.Commands.add("loginCompleto", (user, pw) => {
   cy.get("#username").type(user)
   cy.get("#password").type(pw)
   cy.get("#kc-login").click()
   cy.log("Login realizado com sucesso")
   cy.wait(1000)

   cy.get('input[name="code"]').type("YEWBIGIZ")
   cy.contains("Autenticar").click()
   cy.wait(1000)
})

Cypress.Commands.add("loginSimples", (user, pw) => {
   cy.get("#username").type(user)
   cy.get("#password").type(pw)
   cy.get("#kc-login").click()
   cy.wait(1000)
})

Cypress.Commands.add("selecionarAcesso", (cnpj, perfil) => {
   console.log("Trocando de Perfil --> " + cnpj + " " + perfil)
   cy.get('.user-container > [data-testid="ExpandMoreOutlinedIcon"]').click({
      force: true,
   })
   cy.contains("Meus acessos").click({ force: true })
   cy.contains("tr", cnpj).as("acesso")
   cy.get("@acesso").within(() => {
      cy.get('td:nth-child(4) button:contains("Selecionar")').click()
   })
})

Cypress.Commands.add("preenchaFormulario", (informe, today = false) => {
   if (today) {
      // click on todays date
      cy.get('[data-testid="CalendarIcon"] > path').click({ force: true })
      cy.get(".MuiPickersDay-today").click()
   } else {
      // if not false then use json date
      cy.get('input[name="dataCompetencia"]').type(informe.dataCompetencia, {
         force: true,
      })
   }

   cy.get('input[name="codigoFundo"]').type(informe.codigoFundo)
   cy.get('input[name="codClasse"]').type(informe.codClasse)
   cy.get('input[name="codSubClasse"]').type(informe.codSubClasse)
   cy.get('input[name="valorCota"]').type(informe.valorCota)
   cy.get('input[name="patrimonioLiquido"]').type(informe.patrimonioLiquido)
   cy.get('input[name="volumeTotalAplicacoes"]').type(informe.volumeTotalAplicacoes)
   cy.get('input[name="volumeTotalAplicadoCotas"]').type(informe.volumeTotalAplicadoCotas)
   cy.get('input[name="volumeTotalResgates"]').type(informe.volumeTotalResgates)
   cy.get('input[name="volumeTotalResgateIR"]').type(informe.volumeTotalResgateIR)
   cy.get('input[name="numeroCotistas"]').type(informe.numeroCotistas)
   cy.contains("Submeter").click()
})

Cypress.Commands.add("navegarCadastroManual", () => {
   cy.contains("Informes").click()
   cy.get('a[href="/report"]').click()
   cy.contains("Preencha o formulário").click({ force: true })
   cy.wait(1000)
})

Cypress.Commands.add("pesquisarUsuario", () => {
   cy.get("#filter").click()
   cy.get('[data-value="login"]').click()
   cy.get('input[name="search"]').type("0")
   cy.get('[data-testid="SearchIcon"]').click()
})

Cypress.Commands.add("select2", (get, valor) => {
   cy.get(get).click()
   cy.wait(100)
   cy.get('div[role="presentation"]').contains(valor).click()
})

Cypress.Commands.add("findSolicitacao", (numeroSolicitacao) => {
   console.log("Procurando Registro --> " + numeroSolicitacao)
   let found = false
   cy.wait(1000)

   cy.get("body").then(($body) => {
      const arrowIcon = $body.find('[data-testid="KeyboardArrowRightIcon"]')

      function checkAndClick() {
         if ($body.find(`:contains("${numeroSolicitacao}")`).length > 0 && !found) {
            cy.contains(numeroSolicitacao).click({ force: true })
            found = true
         } else if (!found) {
            cy.get('[data-testid="KeyboardArrowRightIcon"]').click()

            cy.wait(1000).then(checkAndClick)
         }
      }

      checkAndClick()
   })
})

Cypress.Commands.add("preencherTaxas", (funds) => {
   console.log(funds)
   // 2.5 - TAXAS ---------------------------------------------------------------------------------------------
   // 2.5.1 - Administracao
   cy.select2("#cobraTaxaAdministracao", funds.classeTaxaAdm.cobraTaxaAdministracao)
   cy.select2("#perfilTaxaAdministracao", funds.classeTaxaAdm.perfilTaxaAdministracao)
   cy.select2("#unidadeTaxaAdministracao", funds.classeTaxaAdm.unidadeTaxaAdministracao)
   cy.get('input[name="valorTaxaAdministracao"]').type(funds.classeTaxaAdm.valorTaxaAdministracao)
   cy.get('input[name="valorPisoTaxaAdministracao"]').type(funds.classeTaxaAdm.valorPisoTaxaAdministracao)
   cy.get('input[name="valorFixoTaxaAdministracao"]').type(funds.classeTaxaAdm.valorFixoTaxaAdministracao)
   cy.get('input[name="informacoesAdicionaisTaxaAdministracao"]').type(
      funds.classeTaxaAdm.informacoesAdicionaisTaxaAdministracao
   )
   cy.select2("#unidadeTaxaAdministracaoMaxima", funds.classeTaxaAdm.unidadeTaxaAdministracaoMaxima)
   cy.get('input[name="taxaMaximaAdministracao"]').type(funds.classeTaxaAdm.taxaMaximaAdministracao)
   cy.contains("Próximo").click()

   // 2.5.2 - Gestao
   cy.select2("#cobraTaxaGestao", funds.classeTaxaGestao.cobraTaxaGestao)
   cy.select2("#perfilTaxaGestao", funds.classeTaxaGestao.perfilTaxaGestao)
   cy.select2("#unidadeTaxaGestao", funds.classeTaxaGestao.unidadeTaxaGestao)
   cy.get('input[name="valorTaxaGestao"]').type(funds.classeTaxaGestao.valorTaxaGestao)
   cy.get('input[name="valorPisoTaxaGestao"]').type(funds.classeTaxaGestao.valorPisoTaxaGestao)
   cy.get('input[name="valorFixoTaxaGestao"]').type(funds.classeTaxaGestao.valorFixoTaxaGestao)
   cy.get('input[name="informacoesAdicionaisTaxaGestao"]').type(funds.classeTaxaGestao.informacoesAdicionaisTaxaGestao)
   cy.select2("#unidadeTaxaMaximaGestao", funds.classeTaxaGestao.unidadeTaxaMaximaGestao)
   cy.get('input[name="taxaMaximaGestao"]').type(funds.classeTaxaGestao.taxaMaximaGestao)
   cy.contains("Próximo").click()

   // 2.5.3 - Custodia
   cy.select2("#cobraTaxaMaximaCustodia", funds.classeTaxaCustodia.cobraTaxaMaximaCustodia)
   cy.select2("#perfilTaxaMaximaCustodia", funds.classeTaxaCustodia.perfilTaxaMaximaCustodia)
   cy.select2("#unidadeTaxaMaximaCustodia", funds.classeTaxaCustodia.unidadeTaxaMaximaCustodia)
   cy.get('input[name="valorTaxaMaximaCustodia"]').type(funds.classeTaxaCustodia.valorTaxaMaximaCustodia)
   cy.get('input[name="valorPisoTaxaMaximaCustodia"]').type(funds.classeTaxaCustodia.valorPisoTaxaMaximaCustodia)
   cy.get('input[name="valorFixoTaxaMaximaCustodia"]').type(funds.classeTaxaCustodia.valorFixoTaxaMaximaCustodia)
   cy.get('input[name="informacoesAdicionaisCustodia"]').type(funds.classeTaxaCustodia.informacoesAdicionaisCustodia)
   cy.contains("Próximo").click()

   // 2.5.4 - Distribuicao
   cy.select2("#cobraTaxaMaximaDistribuicao", funds.classeTaxaDistribuicao.cobraTaxaMaximaDistribuicao)
   cy.select2("#perfilTaxaMaximaDistribuicao", funds.classeTaxaDistribuicao.perfilTaxaMaximaDistribuicao)
   cy.select2("#unidadeTaxaMaximaDistribuicao", funds.classeTaxaDistribuicao.unidadeTaxaMaximaDistribuicao)
   cy.get('input[name="valorTaxaMaximaDistribuicao"]').type(funds.classeTaxaDistribuicao.valorTaxaMaximaDistribuicao)
   cy.get('input[name="valorPisoTaxaMaximaDistribuicao"]').type(
      funds.classeTaxaDistribuicao.valorPisoTaxaMaximaDistribuicao
   )
   cy.get('input[name="valorFixoTaxaMaximaDistribuicao"]').type(
      funds.classeTaxaDistribuicao.valorFixoTaxaMaximaDistribuicao
   )
   cy.get('input[name="informacoesAdicionaisDistribuicao"]').type(
      funds.classeTaxaDistribuicao.informacoesAdicionaisDistribuicao
   )
   cy.contains("Próximo").click()

   // 2.5.5 - Performance
   cy.select2("#cobraTaxaPerformance", funds.classeTaxaPerformance.cobraTaxaPerformance)
   cy.select2("#perfilTaxaPerformance", funds.classeTaxaPerformance.perfilTaxaPerformance)
   cy.select2("#periodicidadeTaxaPerformance", funds.classeTaxaPerformance.periodicidadeTaxaPerformance)

   cy.get('input[name="informacoesAdicionaisTaxaPerformance"]').type(
      funds.classeTaxaPerformance.informacoesAdicionaisTaxaPerformance
   )
   // Indices de referencia
   cy.get('input[name="indicesReferenciaTaxaPerfomance.0.percentual"]').type(
      funds.classeTaxaPerformance.indicesReferenciaTaxaPerfomancePercentual
   )
   cy.get('input[name="indicesReferenciaTaxaPerfomance.0.percentualIndiceReferencia"]').type(
      funds.classeTaxaPerformance.indicesReferenciaTaxaPerfomanceIndice
   )
   cy.select2(
      "#indicesReferenciaTaxaPerfomance\\.0\\.indiceReferencia",
      funds.classeTaxaPerformance.indicesReferenciaTaxaPerfomanceIndiceRefencia
   )
   cy.get('input[name="indicesReferenciaTaxaPerfomance.0.spread"]').type(
      funds.classeTaxaPerformance.indicesReferenciaTaxaPerfomanceSpread
   )
   cy.contains("Próximo").click()

   // 2.5.6 - Entrada e Saida
   cy.select2("#unidadeTaxaEntrada", funds.classeEntradaSaida.unidadeTaxaEntrada)
   cy.get('input[name="valorTaxaEntrada"]').type(funds.classeEntradaSaida.valorTaxaEntrada)
   cy.select2("#unidadeTaxaSaida", funds.classeEntradaSaida.unidadeTaxaSaida)
   cy.get('input[name="valorTaxaSaida"]').type(funds.classeEntradaSaida.valorTaxaSaida)
   cy.contains("Próximo").click()

   // 2.5.7 - Consultor Especializado
   cy.select2("#perfilTaxaConsultoria", funds.classeTaxaConsultoria.perfil)
   cy.select2("#unidadeTaxaConsultoria", funds.classeTaxaConsultoria.unidade)
   cy.get('input[name="valorTaxaConsultoria"]').type(funds.classeTaxaConsultoria.valor)
   cy.get('input[name="valorPisoTaxaConsultoria"]').type(funds.classeTaxaConsultoria.valorPiso)
   cy.get('input[name="valorFixoTaxaConsultoria"]').type(funds.classeTaxaConsultoria.valorFixo)
   cy.get('input[name="informacoesAdicionaisConsultoria"]').type(funds.classeTaxaConsultoria.informacoesAdicionais)
   cy.select2("#unidadeTaxaMaximaConsultoria", funds.classeTaxaConsultoria.unidadeTaxaMaxima)
   cy.get('input[name="taxaMaximaConsultoria"]').type(funds.classeTaxaConsultoria.taxaMaxima)
   cy.contains("Próximo").click()
})

// ------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------

Cypress.Commands.add("preencherValoresMovimentacoes", (funds) => {
   cy.select2("#indicadorPrazoEmissaoCotas", funds.classeValoresMovimentacoes.indicadorPrazoEmissaoCotas)
   cy.get('input[name="prazoEmissaoCotas"]').type(funds.classeValoresMovimentacoes.prazoEmissaoCotas)
   cy.select2("#indicadorPrazoConversaoResgate", funds.classeValoresMovimentacoes.indicadorPrazoConversaoResgate)
   cy.get('input[name="prazoConversaoResgate"]').type(funds.classeValoresMovimentacoes.prazoConversaoResgate)
   cy.select2("#indicadorPrazoPagamentoResgate", funds.classeValoresMovimentacoes.indicadorPrazoPagamentoResgate)
   cy.get('input[name="prazoPagamentoResgate"]').type(funds.classeValoresMovimentacoes.prazoPagamentoResgate)
   cy.select2("#regraAdicionalPagamentoResgate", funds.classeValoresMovimentacoes.regraAdicionalPagamentoResgate)
   cy.get('input[name="descricaoRegraAdicionalResgate"]').type(
      funds.classeValoresMovimentacoes.descricaoRegraAdicionalResgate
   )
   cy.get('input[name="carenciaInicial"]').type(funds.classeValoresMovimentacoes.carenciaInicial)
   cy.get('input[name="carenciaCiclica"]').type(funds.classeValoresMovimentacoes.carenciaCiclica)
   cy.select2("#indicadorValoresMinimos", funds.classeValoresMovimentacoes.indicadorValoresMinimos)
   cy.get('input[name="valorMinimoAplicacaoInicial"]').type(
      funds.classeValoresMovimentacoes.valorMinimoAplicacaoInicial
   )
   cy.get('input[name="valorMinimoAplicacaoSubsequentes"]').type(
      funds.classeValoresMovimentacoes.valorMinimoAplicacaoSubsequentes
   )
   cy.get('input[name="valorMinimoResgate"]').type(funds.classeValoresMovimentacoes.valorMinimoResgate)
   cy.get('input[name="valorMinimoPermanencia"]').type(funds.classeValoresMovimentacoes.valorMinimoPermanencia)
   cy.contains("Próximo").click()
})

// ------------------------------------------------------------------------------------------------------------------------------------

Cypress.Commands.add("preencherFundo", (fund) => {
   console.log(fund)
   // 1.1 - Dados cadastrais
   cy.select2("#tipoFundo", fund.fundoDadosCadastrais.tipoFundo)
   cy.get('input[name="cnpjFundo"]').type(fund.fundoDadosCadastrais.cnpj)
   cy.get('input[name="razaoSocial"]').type(fund.fundoDadosCadastrais.razaoSocial)
   cy.get('input[name="nomeComercial"]').type(fund.fundoDadosCadastrais.nomeComercial)
   cy.get('input[name="websiteFundo"]').type(fund.fundoDadosCadastrais.websiteFundo)
   cy.get('input[name="dataConstituicao"]').type(fund.fundoDadosCadastrais.dataConstituicao)
   cy.select2("#fundoAdaptado175", fund.fundoDadosCadastrais.fundoAdaptado175)
   cy.contains("Próximo").click()

   // 1.2 - Perfil do fundo
   cy.select2("#prazoDeDuracao", fund.fundoPerfil.prazoDeDuracao)
   cy.get('input[name="duracaoEmMeses"]').type(fund.fundoPerfil.duracaoEmMeses)
   cy.contains("Próximo").click()

   // 1.3 - Prestadores
   for (let prestador in fund.fundoPrestadores) {
      cy.select2("#tipoPrestador", fund.fundoPrestadores[prestador].tipoPrestador)
      cy.get('input[name="cnpjCpf"]').click()
      cy.get('div[role="presentation"]').contains(fund.fundoPrestadores[prestador].cnpjcpf).click()
   }
   cy.contains("Próximo").click()

   for (let documento in fund.fundoDocumentos) {
      cy.select2("#documentType", fund.fundoDocumentos[documento].tipo)
      if (fund.fundoDocumentos[documento].tipo === "Regulamento") {
         cy.get('input[name="dataVigenciaRegulamento"]').type(fund.fundoDocumentos[documento].data)
      }
      cy.get('input[type="file"]').selectFile(fund.fundoDocumentos[documento].documento, {
         force: true,
      })
   }

   cy.contains("Concluir fundo casca").click({ force: true })
   cy.wait(1000)
})

// fixing preencherfundo  with proper json
Cypress.Commands.add("preencherFundo2", (fundo) => {
   console.log(fundo)
   // 1.1 - Dados cadastrais
   cy.select2("#tipoFundo", fundo.dadosCadastrais.tipoFundo)
   cy.get('input[name="cnpjFundo"]').type(fundo.dadosCadastrais.cnpj)
   cy.get('input[name="razaoSocial"]').type(fundo.dadosCadastrais.razaoSocial)
   cy.get('input[name="nomeComercial"]').type(fundo.dadosCadastrais.nomeComercial)
   cy.get('input[name="websiteFundo"]').type(fundo.dadosCadastrais.websiteFundo)
   cy.get('input[name="dataConstituicao"]').type(fundo.dadosCadastrais.dataConstituicao)
   cy.select2("#fundoAdaptado175", fundo.dadosCadastrais.fundoAdaptado175)
   cy.contains("Próximo").click()

   // 1.2 - Perfil do fundo
   cy.select2("#prazoDeDuracao", fundo.perfil.prazoDeDuracao)
   cy.get('input[name="duracaoEmMeses"]').type(fundo.perfil.duracaoEmMeses)
   cy.contains("Próximo").click()

   // 1.3 - Prestadores
   for (let prestador in fundo.prestadores) {
      cy.select2("#tipoPrestador", fundo.prestadores[prestador].tipoPrestador)
      cy.get('input[name="cnpjCpf"]').click()
      cy.get('div[role="presentation"]').contains(fundo.prestadores[prestador].cnpjcpf).click()
   }
   cy.contains("Próximo").click()
   // 1.4 - Documentos
   for (let documento in fundo.documentos) {
      cy.select2("#documentType", fundo.documentos[documento].tipo)
      if (fundo.documentos[documento].tipo === "Regulamento") {
         cy.get('input[name="dataVigenciaRegulamento"]').type(fundo.documentos[documento].data)
      }
      cy.get('input[type="file"]').selectFile(fundo.documentos[documento].documento, {
         force: true,
      })
      cy.wait(1000)
   }

   cy.contains("Concluir fundo casca").click({ force: true })
   cy.wait(1000)
})

// classe complementada
Cypress.Commands.add("preencherClasse", (fundClass, classePerfil) => {
   cy.get('input[name="cnpjClasse"]').type(fundClass.classeDadosCadastrais.cnpjClasse) // mesmo CNPJ gerado acima
   cy.select2("#evento", fundClass.classeDadosCadastrais.evento)
   cy.get('input[name="razaoSocial"]').type(fundClass.classeDadosCadastrais.razaoSocial)
   cy.get('input[name="nomeComercial"]').type(fundClass.classeDadosCadastrais.nomeComercial)
   cy.get('input[name="dataConstituicao"]').type(fundClass.classeDadosCadastrais.dataConstituicao)
   cy.get('input[name="inicioAtividade"]').type(fundClass.classeDadosCadastrais.inicioAtividade)
   cy.contains("Próximo").click()

   // 2.2 - Perfil da classe
   for (const field in classePerfil) {
      const value = classePerfil[field]
      cy.select2(`#${field}`, value)
   }
   cy.contains("Próximo").click()

   // 2.3 - Prestadores - classe
   for (let prestador in fundClass.classePrestadores) {
      cy.select2("#tipoPrestador", fundClass.classePrestadores[prestador].tipoPrestador)
      cy.get('input[name="cnpjCpf"]').click()
      cy.get('div[role="presentation"]').contains(fundClass.classePrestadores[prestador].cnpjcpf).click()
      cy.wait(500)
   }

   cy.contains("Concluir classe").click()
   cy.wait(1000)
   cy.contains("Preencher complemento").click()

   // 2.4 - Perfil complementar
   cy.select2("#restricaoInvestimento", fundClass.classePerfilComplementar.restricaoInvestimento)
   cy.select2("#opcoesDeRestrito", fundClass.classePerfilComplementar.opcoesDeRestrito)
   cy.select2("#tipoInvestidor", fundClass.classePerfilComplementar.tipoInvestidor)
   cy.select2("#fundoAlavancado", fundClass.classePerfilComplementar.fundoAlavancado)
   cy.select2("#multiGestor", fundClass.classePerfilComplementar.multiGestor)
   cy.select2("#indicadorAplicacaoAutomatica", fundClass.classePerfilComplementar.indicadorAplicacaoAutomatica)
   cy.select2("#previdenciario", fundClass.classePerfilComplementar.previdenciario)
   cy.select2("#planoPrevidencia", fundClass.classePerfilComplementar.planoPrevidencia)
   cy.select2("#periodicidadeEnvioPLCota", fundClass.classePerfilComplementar.periodicidadeEnvioPLCota)
   cy.select2("#calculoCota", fundClass.classePerfilComplementar.calculoCota)
   cy.select2("#abertoCaptacao", fundClass.classePerfilComplementar.abertoCaptacao)
   cy.contains("Próximo").click()

   // 2.5 - TAXAS ---------------------------------------------------------------------------------------------
   // 2.5.1 - Administracao
   cy.preencherTaxas(fundClass)

   // 2.6 - Val. mínimos e movimentações
   cy.preencherValoresMovimentacoes(fundClass)

   // 2.7 - Prestadores Complementares
   for (let prestador in fundClass.classePrestadoresComplementares) {
      cy.select2("#tipoPrestador", fundClass.classePrestadoresComplementares[prestador].tipoPrestador)
      cy.get('input[name="cnpjCpf"]').click()
      cy.get('div[role="presentation"]').contains(fundClass.classePrestadoresComplementares[prestador].cnpjcpf).click()
   }
   cy.contains("Próximo").click()
})
// ---------------------------------------------------------------------------------------
// classe simples
Cypress.Commands.add("preencherClasseSimples", (fundo) => {
   cy.get('input[name="cnpjClasse"]').type(fundo.fundo.classe.dadosCadastrais.cnpjClasse)
   cy.select2("#evento", fundo.fundo.classe.dadosCadastrais.evento)
   cy.get('input[name="razaoSocial"]').type(fundo.fundo.classe.dadosCadastrais.razaoSocial)
   cy.get('input[name="nomeComercial"]').type(fundo.fundo.classe.dadosCadastrais.nomeComercial)
   cy.get('input[name="dataConstituicao"]').type(fundo.fundo.classe.dadosCadastrais.dataConstituicao)
   cy.get('input[name="inicioAtividade"]').type(fundo.fundo.classe.dadosCadastrais.inicioAtividade)
   cy.contains("Próximo").click()

   // 2.2 - Perfil da classe
   for (const field in fundo.fundo.classe.perfil) {
      const value = fundo.fundo.classe.perfil[field]
      cy.select2(`#${field}`, value)
   }
   cy.contains("Próximo").click()

   // 2.3 - Prestadores - classe
   for (let prestador in fundo.fundo.classe.prestadores) {
      cy.select2("#tipoPrestador", fundo.fundo.classe.prestadores[prestador].tipoPrestador)
      cy.get('input[name="cnpjCpf"]').click()
      cy.get('div[role="presentation"]').contains(fundo.fundo.classe.prestadores[prestador].cnpjcpf).click()
      cy.wait(500)
   }

   cy.contains("Concluir classe").click()
   cy.wait(1000)
   cy.contains("Cadastrar subclasse").click()
})

Cypress.Commands.add("preencherSubClasse", (fundo) => {
   // 3. SUBCLASSE -----------------------------------------------------------------------------------------------------------------

   // 3.1 Dados cadastrais - subclasse
   cy.get('input[name="nomeComercial"]').type(fundo.fundo.classe.subClasse.dadosCadastrais.nomeComercial)
   cy.get('input[name="dataConstituicao"]').type(fundo.fundo.classe.subClasse.dadosCadastrais.dataConstituicao)
   cy.contains("Próximo").click()

   // 3.2 - Perfil da subclasse
   cy.select2("#restricaoInvestimento", fundo.fundo.classe.subClasse.perfil.restricaoInvestimento)
   cy.select2("#opcoesDeRestrito", fundo.fundo.classe.subClasse.perfil.opcoesDeRestrito)
   cy.select2("#tipoInvestidor", fundo.fundo.classe.subClasse.perfil.tipoInvestidor)
   cy.select2("#fundoAlavancado", fundo.fundo.classe.subClasse.perfil.fundoAlavancado)
   cy.select2("#multiGestor", fundo.fundo.classe.subClasse.perfil.multiGestor)
   cy.select2("#indicadorAplicacaoAutomatica", fundo.fundo.classe.subClasse.perfil.indicadorAplicacaoAutomatica)
   cy.select2("#previdenciario", fundo.fundo.classe.subClasse.perfil.previdenciario)
   cy.select2("#planoPrevidencia", fundo.fundo.classe.subClasse.perfil.planoPrevidencia)
   cy.select2("#periodicidadeEnvioPLCota", fundo.fundo.classe.subClasse.perfil.periodicidadeEnvioPLCota)
   cy.select2("#formaCondominio", fundo.fundo.classe.subClasse.perfil.formaCondominio)
   cy.select2("#calculoCota", fundo.fundo.classe.subClasse.perfil.calculoCota)
   cy.select2("#abertoCaptacao", fundo.fundo.classe.subClasse.perfil.abertoCaptacao)
   cy.contains("Próximo").click()

   // 3.3 - Taxas - subclasse
   cy.preencherTaxas(fundo.fundo.classe.subClasse.taxas)

   // 3.4 - Val. mínimos e movimentações - subclasse
   cy.preencherValoresMovimentacoes(fundo.fundo.classe.subClasse)

   // 3.5 - Prestadores - subclasse
   for (let prestador in fundo.fundo.classe.subClasse.subclassePrestadores) {
      cy.select2("#tipoPrestador", fundo.fundo.classe.subClasse.subclassePrestadores[prestador].tipoPrestador)
      cy.get('input[name="cnpjCpf"]').click()
      cy.get('div[role="presentation"]')
         .contains(fundo.fundo.classe.subClasse.subclassePrestadores[prestador].cnpjcpf)
         .click()
   }
   cy.contains("Concluir subclasse").click()
})
