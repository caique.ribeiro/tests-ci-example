import "./commands.js"
import { subDays, startOfWeek, isWeekend, isSaturday, isFriday } from "date-fns"

export const getNextValidWorkDay = () => {
   const today = new Date()
   console.log(today)

   if (isFriday(today)) {
      today.setDate(today.getDate() + 3)
      console.log("tomorrow is saturday")
   } else if (isSaturday(today)) {
      today.setDate(today.getDate() + 2)
      console.log("tomorrow is sunday")
   } else {
      today.setDate(today.getDate() + 1)
      console.log("tomorrow is not the weekend")
   }
   const formattedDate = formatDate(today)
   console.log(formattedDate)
   return formattedDate
}

export const getNonWorkDay = () => {
   const today = new Date()

   if (isWeekend(today)) {
      console.log("today is weekend")
      return formatDate(today)
   } else {
      console.log("today is not the weekend")
      const lastSunday = subDays(startOfWeek(today), 0)
      return formatDate(lastSunday)
   }
}

export const formatDate = (date, options = {}) => {
   return date.toLocaleDateString("pt-BR", options).replace(/-|\/|_|\.|,|\/|\(|\)|\s/g, "")
}

export const generateRandomHash = () => {
   const letters = [...Array(2)].map(() => String.fromCharCode(65 + Math.floor(Math.random() * 26))).join("")
   const numbers = [...Array(3)].map(() => Math.floor(Math.random() * 10)).join("")
   return `${letters}${numbers}`
}
